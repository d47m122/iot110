#!/usr/bin/python
import json
import logging
import time
from sense_hat import SenseHat
from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTShadowClient
from threading import Thread
from flask import *

class shadowCallbackContainer:
    def __init__(self, deviceShadowInstance):
        self.deviceShadowInstance = deviceShadowInstance

    def customShadowCallback_Delta(self, payload, responseStatus, token):
        print("\n\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        print("MQTT: Received a delta message:")
        print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n")
        payloadDict = json.loads(payload)
        deltaMessage = json.dumps(payloadDict["state"])
        print("\n\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        print("MQTT: " + deltaMessage)
        if payloadDict["state"]["message"]:
            message = payloadDict["state"]["message"]
            printSenseMessage(message)
        print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n")
        print("\n\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        print("MQTT: Request to update the reported state...")
        newPayload = '{"state":{"reported":' + deltaMessage + '}}'
        self.deviceShadowInstance.shadowUpdate(newPayload, None, 5)
        print("MQTT: Sent " + str(newPayload))
        print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n")

def printSenseMessage(message):
    sense = SenseHat()
    sense.show_message(message)

# Custom Shadow callback
def customShadowCallback_Update(payload, responseStatus, token):
	# payload is a JSON string ready to be parsed using json.loads(...)
	# in both Py2.x and Py3.x
	if responseStatus == "timeout":
		print("MQTT: Update request " + token + " time out!")
	if responseStatus == "accepted":
		payloadDict = json.loads(payload)
		print("\n\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
		print("MQTT: Update request with token: " + token + " accepted!")
		print("temperature: " + str(payloadDict["state"]["reported"]["temperature"]))
        print("humidity: " + str(payloadDict["state"]["reported"]["humidity"]))
        print("pressure: " + str(payloadDict["state"]["reported"]["pressure"]))
        print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n")
	if responseStatus == "rejected":
		print("MQTT: Update request " + token + " rejected!")

# Configure logging
logger = logging.getLogger("AWSIoTPythonSDK.core")
logger.setLevel(logging.DEBUG)
streamHandler = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
streamHandler.setFormatter(formatter)
logger.addHandler(streamHandler)

#AWS MQTT parameters
host = "abe6h2831naar.iot.us-west-2.amazonaws.com"
rootCAPath = "root-CA.crt"
certificatePath = "iotDylan.cert.pem"
privateKeyPath = "iotDylan.private.key"

# Init AWSIoTMQTTShadowClient
myAWSIoTMQTTShadowClient = None
myAWSIoTMQTTShadowClient = AWSIoTMQTTShadowClient("iotDylanThingShadow")
myAWSIoTMQTTShadowClient.configureEndpoint(host, 8883)
myAWSIoTMQTTShadowClient.configureCredentials(rootCAPath, privateKeyPath, certificatePath)

# AWSIoTMQTTShadowClient configuration
myAWSIoTMQTTShadowClient.configureAutoReconnectBackoffTime(1, 32, 20)
myAWSIoTMQTTShadowClient.configureConnectDisconnectTimeout(10)  # 10 sec
myAWSIoTMQTTShadowClient.configureMQTTOperationTimeout(5)  # 5 sec

# Connect to AWS IoT
myAWSIoTMQTTShadowClient.connect()

# Create a deviceShadow with persistent subscription
iotDylanShadow = myAWSIoTMQTTShadowClient.createShadowHandlerWithName("iotDylan", True)
shadowCallbackContainer_iotDylanShadow = shadowCallbackContainer(iotDylanShadow)

iotDylanShadow.shadowRegisterDeltaCallback(shadowCallbackContainer_iotDylanShadow.customShadowCallback_Delta)

sense = SenseHat()

while True:
    mbToIn = sense.get_pressure() / 33.8638866667
    JSONPayload = '{"state":{"reported":{"temperature":' + "{0:.1f}".format(sense.get_temperature()) + ',"pressure":' + "{0:.1f}".format(mbToIn) + ',"humidity":' + "{0:.1f}".format(sense.get_humidity()) + '}}}'
    iotDylanShadow.shadowUpdate(JSONPayload, customShadowCallback_Update, 5)
    print("\n\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
    print("SENSORS: Next sensor push to AWS in mins.")
    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n")
    time.sleep(300)